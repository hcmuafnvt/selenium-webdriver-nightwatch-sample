GETTING STARTED :

- To setting up and run the project, please follow the steps listed below :
1. Install chromedriver :
   > npm install chromedriver -g
2. Install packages
   > npm install
3. Run test
   > npm test

If you want to test with multiple browsers concurrency, please edit the file package.json as below (separate each browser by comma notation):

Replace from : "./nightwatch -c ./nightwatch.config.json --env chrome" (only test with chrome) to : "./nightwatch -c ./nightwatch.config.json --env default,chrome" (test with firefox and chrome)





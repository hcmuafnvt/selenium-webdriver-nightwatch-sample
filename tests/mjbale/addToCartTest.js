module.exports = {
   'step 1 : go to homepage mjbale.com' : function(browser) {
      browser
         .url('http://www.mjbale.com/')
         .getTitle(function(title) {
            this.assert.equal(title, "Men's Suits, Tuxedos, Shirts & Fashion | MJ Bale");
         })
   },
   'step 2 : go to category New Arrivals' : function(browser) {
      browser
         .assert.attributeContains('.menu-category li:first-child a', 'href', '/new-arrivals')
         .click('.menu-category li:first-child a')
         .pause(1000)
         .assert.title('New Arrivals')
   },
   'step 3 : go to PDP page' : function(browser) {
      browser
         .assert.containsText('#search-result-items li:first-child .product-tile .product-name a', 'HEWITT SAND JACKET')
         .getAttribute('#search-result-items li:first-child .product-tile .product-name a', 'href', function(result) {
            this.url(result.value);
         });
      browser.assert.elementPresent('.swatches.size');
      browser
         .click('.swatches.size li:first-child a')
         .pause(500)
         .setValue('select.quantity-dropdown',  '2')
         .pause(500)
   },
   'step 4 : add to cart' : function(browser) {
      browser.expect.element('#add-to-cart').to.be.enabled;
      browser
         .click('#add-to-cart')
         .pause(3000)
         .end();
   }
};
